#cloud-config
users:
  - name: mike
    groups:
      - sudo
      - docker
    sudo: ["ALL=(ALL) NOPASSWD:ALL"]
    shell: /bin/bash
    home: /home/mike
    lock_passwd: true
    ssh-authorized-keys:
      - ${userdata_sshkey}

disable_root: true

apt_update: true
package_update: true
package_upgrade: true
package_reboot_if_required: true
packages:
  - git
  - docker.io
  - docker-compose

write_files:
  - encoding: b64
    content: ${letsencrypt_nginx_custom_conf}
    path: /opt/letsencrypt-nginx/custom.conf
  - encoding: b64
    content: ${letsencrypt_nginx_compose}
    path: /opt/letsencrypt-nginx/docker-compose.yml
  - encoding: b64
    content: ${letsencrypt_nginx_service}
    path: /etc/systemd/system/letsencrypt-nginx.service
  - encoding: b64
    content: ${project_service}
    path: /etc/systemd/system/project.service
  - encoding: b64
    content: ${project_compose}
    path: /opt/project/docker-compose.yml
  - encoding: b64
    content: ${project_env}
    path: /opt/project/.env

runcmd:
  - systemctl start letsencrypt-nginx
  - systemctl start project
