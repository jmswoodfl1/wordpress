output "my_ssh_key" {
  value = var.ssh_key

}
output "my_user_data" {
  value = data.template_file.setup.rendered
}