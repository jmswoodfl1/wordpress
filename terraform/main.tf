terraform {
  backend "remote" {
    organization = "Tampa-bay-tech-solutions"

    workspaces {
      name = "wordpress-site"
    }
  }
}

provider "digitalocean" {
  token = var.token
}

resource "digitalocean_droplet" "myserver" {
  name      = "myserver"
  size      = "s-1vcpu-1gb"
  image     = "ubuntu-20-04-x64"
  region    = "nyc3"
  user_data = data.template_file.setup.rendered
}
#pound sign is a comment, this names the project
resource "digitalocean_project" "wordpress" {
  name        = "wordpress"
  description = "My first self hosted wordpress site"
  purpose     = "Wordpress Website"
  environment = "Development"
  resources   = [digitalocean_droplet.myserver.urn]
}
#define domain resource
resource "digitalocean_domain" "my_wordpress_site" {
  name       = "pinellassbs.com"
  ip_address = digitalocean_droplet.myserver.ipv4_address
}
data "template_file" "setup" {
  template = file("${path.module}/template/main.tpl")

  vars = {
    userdata_sshkey               = var.ssh_key
    letsencrypt_nginx_compose     = base64encode(file("${path.module}/template/letsencrypt-nginx/docker-compose.yml"))
    letsencrypt_nginx_service     = base64encode(file("${path.module}/template/letsencrypt-nginx/letsencrypt-nginx.service"))
    letsencrypt_nginx_custom_conf = base64encode(file("${path.module}/template/letsencrypt-nginx/custom.conf"))
    project_compose               = base64encode(file("${path.module}/template/docker-compose.yml"))
    project_env                   = base64encode(data.template_file.environment_file.rendered)
    project_service               = base64encode(file("${path.module}/template/project.service"))
  }
}
data "template_file" "environment_file" {
  template = file("${path.module}/template/env.tpl")

  vars = {
    DOMAIN = var.domain
  }
}