SHELL=/bin/bash

plan:
	@cd terraform && \
	TF_CLI_CONFIG_FILE=./.terraformrc terraform plan && \
	cd ..

setup:
	@cd terraform && \
	TF_CLI_CONFIG_FILE=./.terraformrc terraform init && \
	cd ..

apply:
	@cd terraform && TF_CLI_CONFIG_FILE=./.terraformrc terraform apply -auto-approve -var "ssh_key=${SSH_KEY}" && \
	cd ..

destroy:
	@cd terraform && TF_CLI_CONFIG_FILE=./.terraformrc terraform destroy -auto-approve -var "ssh_key=${SSH_KEY}" && cd ..